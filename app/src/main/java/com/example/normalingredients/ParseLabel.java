package com.example.normalingredients;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.Text;
import java.io.IOException;
import java.util.List;

public class ParseLabel  extends AppCompatActivity {

    List<LabelText> labelTextsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Uri> imgUriList = (List<Uri>) getIntent().getSerializableExtra("pictureUriList");
        if (imgUriList.size() == 0)
        {
            Log.d("ParseLabel", "No images to parse");
            setResult(RESULT_CANCELED);
            finish();
        }

        for (Uri uri : imgUriList)
        {
            InputImage image;
            try {
                image = InputImage.fromFilePath(getBaseContext(), uri);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
            TextRecognition.getClient().process(image).addOnSuccessListener(new OnSuccessListener<Text>() {
                @Override
                public void onSuccess(Text text) {
                    // On Success do something
                    LabelText imgLabelText = new LabelText();
                    extractLabelInfo(text, imgLabelText);
                    return;
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Do Something else
                    Log.e("ParseLabel", "Unable to process inputImage");
                }
            });
        }
    }

    private boolean extractLabelInfo(Text result, LabelText labelTextStruct) {
        // Populate the labelText data structure and return success or failure at end
        String resultText = result.getText();
        for (Text.TextBlock block : result.getTextBlocks()) {
            String blockText = block.getText();
            Log.d("blockText" ,blockText);
            for (Text.Line line : block.getLines()) {
                String lineText = line.getText();
                Log.d("line", lineText);
                if (lineText.contains("per")  || lineText.contains("pour")) {
                    int openBracketIndex = lineText.indexOf('(');
                    int closeBracketIndex = lineText.indexOf(')');
                    String servingSizeSubstring = lineText.substring(openBracketIndex,closeBracketIndex);
                    // Take the substring, split by space and convert number string into double to be stored in data struct
                    labelTextStruct.servingSizeVal =  Double.parseDouble(servingSizeSubstring.split(" ")[0]);

                    if (servingSizeSubstring.contains("g"))
                    {
                        labelTextStruct.servingSizeUnit = LabelText.servingSizeUnitEnum.g;
                    }
                    else if (servingSizeSubstring.contains("kg")) {
                        labelTextStruct.servingSizeUnit = LabelText.servingSizeUnitEnum.kg;
                    }
                    else if (servingSizeSubstring.contains("mL")) {
                        labelTextStruct.servingSizeUnit = LabelText.servingSizeUnitEnum.mL;
                    }
                    else if (servingSizeSubstring.contains("L")) {
                        labelTextStruct.servingSizeUnit = LabelText.servingSizeUnitEnum.L;
                    }
                }
            }
        }
        Log.d("parseLabel", String.valueOf(labelTextStruct.servingSizeVal) + labelTextStruct.servingSizeUnit);
        return true;
    }

}
