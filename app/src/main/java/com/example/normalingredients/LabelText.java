package com.example.normalingredients;

public class LabelText {
    // servingSizeVal and servingSizeUnit are put together to get whole service size
    public double servingSizeVal;
    enum servingSizeUnitEnum {
        g,
        kg,
        mL,
        L,
    }
    public servingSizeUnitEnum servingSizeUnit;

    public double calories;
    public double fat;
    public double carbohydrates;
    public double protein;
}
