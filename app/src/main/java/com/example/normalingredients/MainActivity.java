package com.example.normalingredients;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.util.concurrent.ListenableFuture;

import androidx.annotation.NonNull;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.os.Environment;
import android.os.storage.StorageManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};

    private FloatingActionButton compareFab;
    private FloatingActionButton capturePictureFab;

    private PreviewView previewView;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private ImageCapture imageCapture;

    private List<Uri> pictureUriList = new ArrayList<Uri>();
    private static final int PARSE_LABEL_RESULT_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        compareFab = findViewById(R.id.compare_fab);
        capturePictureFab = findViewById(R.id.capture_fab);
        capturePictureFab.setEnabled(false);
//        compareFab.setEnabled(false);

        compareFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Comparing labels", Toast.LENGTH_SHORT).show();

                // Start the parseLabel activity
                Intent intent = new Intent(view.getContext(), ParseLabel.class);
                intent.putExtra("pictureUriList", (Serializable)pictureUriList);
                startActivityForResult(intent, PARSE_LABEL_RESULT_CODE);
            }
        });

        // Asking for permission for camera and writing to storage
        if (!allPermissionsGranted())
        {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        // Add connects previewView to UI
        previewView = findViewById(R.id.preview_view);
        // returns the cameraProviderFuture which I can listen to for completion and then bind use case to LifecycleOwner
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        startCamera();
    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS)
        {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        capturePictureFab.setEnabled(true);

        capturePictureFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Run function to save photo and change activity to allow user to confirm photo
                Toast.makeText(getBaseContext(), "Clicked capture", Toast.LENGTH_SHORT).show();
                takePhoto();
            }
        });
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (!allPermissionsGranted()) {
                ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
            }
        }
    }

    // Binds the cameraProvider to a preview and places onto the previewView UI widget
    private void startCamera() {
        // Once it finds the cameraProvider is done, go into the try/catch
        cameraProviderFuture.addListener(() -> {
            try {
                // Get the cameraProvider which is ready
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                // builds the preview and binds the cameraProvider to the use case lifecycle
                buildUseCasesAndBindToLifecycle(cameraProvider);
            } catch (ExecutionException | InterruptedException e) {
                // Should never be reached
                Log.e("camera","listener for cameraProviderFuture failed");
            }
        }, ContextCompat.getMainExecutor(this) );
    }

    private void buildUseCasesAndBindToLifecycle(@NonNull ProcessCameraProvider cameraProvider) {
        // Build a new preview
        Preview preview = new Preview.Builder().build();
        // Configure the camera and build it
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();
        // Configure the object to capture an picture
        imageCapture = new ImageCapture.Builder()
                .setTargetRotation(previewView.getDisplay().getRotation())
                .build();
        // Bind the cameraProvider since we now have it available and bind to the this lifecycle with the configurations and use cases (capture and preview)
        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, imageCapture, preview);
        // Connect the cameraProvider to the previewView created in the content_main.xml
        preview.setSurfaceProvider(previewView.createSurfaceProvider(camera.getCameraInfo()));
    }

    private void takePhoto() {
        // Create file in the internal storage
        File directory = getApplicationContext().getFilesDir();
        // Add timestamp to file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File fileToWriteTo  = new File(directory, "IMG_" + timeStamp + ".jpg");

        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(fileToWriteTo).build();
        Executor cameraExecutor = Executors.newSingleThreadExecutor();
        imageCapture.takePicture(outputFileOptions, cameraExecutor,  new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                // Send to the review activity
                Uri fileUri = Uri.fromFile(fileToWriteTo);
                pictureUriList.add(fileUri);
                Log.d("camera", "Saved file at URI: " + fileUri);


//                compareFab = findViewById(R.id.compare_fab);
//                compareFab.setEnabled(true);
//                boolean compareFabEn = compareFab.isEnabled();
//                Log.d("compareFab", "The compareFab enabled is: " + compareFabEn);

//                Ensuring there's enough space on the device for the photos about to be taken
//                StorageManager storageMgr = null;
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//                    storageMgr = getApplicationContext().getSystemService(StorageManager.class);
//                    UUID appSpecificInternalDirUuid = null;
//                    long bytesAvail = 0;
//                    long cacheAvail = 0;
//                    try {
//                        appSpecificInternalDirUuid = storageMgr.getUuidForPath(getFilesDir());
//                        bytesAvail = storageMgr.getAllocatableBytes(appSpecificInternalDirUuid);
//                        cacheAvail = storageMgr.getCacheQuotaBytes(appSpecificInternalDirUuid);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    Log.d("camera", "Bytes free: " + bytesAvail + "Cache free: " + cacheAvail);
////                    testFiles();
//                }
            }
            @Override
            public void onError(@NonNull ImageCaptureException error) {
                Log.e("camera", "Image failed to save in takePhoto()");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (PARSE_LABEL_RESULT_CODE): {
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getBaseContext(), "Please take a picture", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
